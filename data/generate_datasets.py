import os
import re
import pwd
import argparse
import collections

def get_root_filenames_in_folder(folder_path):
    root_filenames = []
    for filename in os.listdir(folder_path):
        if os.path.isfile(os.path.join(folder_path, filename)) and filename.endswith('.root'):
            root_filenames.append(filename)
    return root_filenames

def extract_pu_and_id_values(root_filenames):
    pu_id_values = []
    pattern = r'PU_(\d+\.\d+)\.(\d+)'
    
    for filename in root_filenames:
        match = re.search(pattern, filename)
        if match:
            pu_value = float(match.group(1))
            id_value = int(match.group(2))
            pu_id_values.append((pu_value, id_value))
    
    return pu_id_values


def get_folder_owner_username(folder_path):
    folder_stat = os.stat(folder_path)
    user_id = folder_stat.st_uid
    user_info = pwd.getpwuid(user_id)
    return user_info.pw_name

def group_filenames_by_pu(root_filenames, pu_values):
    pu_filename_mapping = collections.defaultdict(list)
    
    for filename, (pu_value, id_value) in zip(root_filenames, pu_values):
        pu_filename_mapping[pu_value].append((filename, id_value))
    
    return pu_filename_mapping


parser = argparse.ArgumentParser(
                    prog='gererate_datasets',
                    description='generates the dataset files used for bril_it_analysis')

parser.add_argument("directory", type=str, help="the directory where de root files can be found")
parser.add_argument("-o ","--output", type=str, help="where to export the .txt files to", default="./")
parser.add_argument("-n ","--nEntries", type=int, help="maximum number of entries per txt file. (default 10)", default=10)
parser.add_argument("-g ","--geometry", type=str, help="the geometry used for the data", default="D91")

args = parser.parse_args()

root_files = get_root_filenames_in_folder(args.directory)
pu_and_id_values = extract_pu_and_id_values(root_files)
owner = get_folder_owner_username(args.directory)

sorted_data = group_filenames_by_pu(root_files, pu_and_id_values)

for key in sorted_data.keys():
    written_all_data = False
    n = 0
        
    while(not written_all_data):
        
        min_i = n * args.nEntries
        max_i = (n+1) * args.nEntries if (n+1) * args.nEntries < len(sorted_data[key]) else len(sorted_data[key])        
        written_all_data = True if len(sorted_data[key]) <= max_i else False
        
        with open(args.output + "/PU_" + format(key, "0.1f") + "_" + args.geometry + "_" + owner + "." + str(n) + ".txt", "w") as file:
            for (file_name, file_id) in sorted_data[key][min_i:max_i]:
                file.write(args.directory + "/" + file_name + "\n")
        n += 1

            
    
