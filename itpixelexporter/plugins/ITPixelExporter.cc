// -*- C++ -*-
//
// Package:    ITsim/ITdigiExporter
// Class:      ITdigiExporter
//
/**\class ITdigiExporter ITdigiExporter.cc
ITsim/ITdigiExporter/plugins/ITdigiExporter.cc

Description: [one line class summary]

Implementation:
[Notes on implementation]
*/
//
// Original Author:  Georg Auzinger
//         Created:  Thu, 17 Jan 2032 13:12:52 GMT
// Edits: Max Bensink
//
//

// #define DEBUG
// #define REMOVE_EDGES_AND_CENTER

// user include files
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "DataFormats/SiPixelCluster/interface/SiPixelCluster.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/ESInputTag.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "Geometry/CommonDetUnit/interface/GeomDet.h"
#include "Geometry/CommonDetUnit/interface/PixelGeomDetUnit.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"

#include "DataFormats/Common/interface/DetSetVector.h"
#include "DataFormats/Common/interface/DetSetVectorNew.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/DetId/interface/DetId.h"
#include "DataFormats/SiPixelDetId/interface/PixelSubdetector.h"
#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"
#include "DataFormats/TrackerCommon/interface/TrackerTopology.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "CommonTools/Utils/interface/TFileDirectory.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "CondFormats/DataRecord/interface/TrackerDetToDTCELinkCablingMapRcd.h"
#include "CondFormats/SiPhase2TrackerObjects/interface/DTCELinkId.h"
#include "CondFormats/SiPhase2TrackerObjects/interface/TrackerDetToDTCELinkCablingMap.h"

#include <TTree.h>

constexpr int32_t ROW_MAX = 1354;
constexpr int32_t COL_MAX = 434;
constexpr int32_t ROW_CUTOFF = 10;
constexpr int32_t COL_CUTOFF = 2;
constexpr int32_t ACTUAL_ROW_MAX = ROW_MAX - ROW_CUTOFF;
constexpr int32_t ACTUAL_COL_MAX = COL_MAX - COL_CUTOFF;

constexpr int32_t SECOND_CHIP_START_COL = ACTUAL_COL_MAX / 2;
constexpr int32_t SECOND_CHIP_START_ROW = ACTUAL_ROW_MAX / 2;

typedef std::pair<int, int> vertex_t;

struct ITDigiModule
{
  // side of the collider (1 == left and 2 == right)
  uint8_t side;

  // disk per side (max 4)
  uint8_t disk;

  // module per ring (max 25)
  uint8_t module;

  // layer per disk (1 == front and 2 == back)
  uint8_t layer;

  // ring per disk (max 5)
  uint8_t ring;

  // number of clusters per module
  uint32_t hlt_nclusters;

  // number of clusters per module
  uint32_t nclusters;

  // connected elink
  uint8_t elink;
  uint8_t lpgbt;
  uint16_t dtc;

  // id of module
  uint64_t detid;

  std::vector<unsigned short> row;    // The pixel row number on the module
  std::vector<unsigned short> column; // The pixel column number on the module
  std::vector<unsigned char> adc;     // ADC value for given pixel
};

std::vector<std::tuple<uint32_t, uint32_t>>
    backside_panel_ids({{348655e3, 348680e3},
                        {349175e3, 349205e3},
                        {349703e3, 349723e3},
                        {350227e3, 350248e3},
                        {357042e3, 357063e3},
                        {357567e3, 357588e3},
                        {358091e3, 358112e3},
                        {358616e3, 358637e3}});

struct ITDigiEvent
{
  // Struct to store address and ADC information about all Digis in an event

  long int event;
  unsigned int nmodule;

  TTree *tree = NULL;
  std::vector<ITDigiModule> modules; // The module number

  struct Branches
  {
    std::vector<unsigned int> detid;
    std::vector<unsigned char> module;
    std::vector<unsigned char> disk;
    std::vector<unsigned char> layer;
    std::vector<unsigned char> ring;

    std::vector<uint8_t> side;
    std::vector<uint8_t> elink;
    std::vector<uint8_t> lpgbt;
    std::vector<uint16_t> dtc; 

    std::vector<std::vector<unsigned short>> row;    // The pixel row number on the module
    std::vector<std::vector<unsigned short>> column; // The pixel column number on the module
    std::vector<std::vector<unsigned char>> adc;     // ADC value for given pixel
  } branches;

  void clear()
  {
    this->event = -1;
    this->nmodule = 0;
    this->modules.clear();

    branches = Branches();
  }

  void attach_tree(TTree *tree)
  {
    this->tree = tree;
    tree->Branch("event", &this->event, "event/I");
    tree->Branch("nmodule", &this->nmodule, "nmodule/I");

    tree->Branch("detid", &branches.detid);
    tree->Branch("module", &branches.module);
    tree->Branch("disk", &branches.disk);
    tree->Branch("layer", &branches.layer);
    tree->Branch("ring", &branches.ring);

    tree->Branch("side", &branches.side);
    tree->Branch("elink", &branches.elink);
    tree->Branch("lpgbt", &branches.lpgbt);
    tree->Branch("dtc", &branches.dtc);

    tree->Branch("row", &branches.row);
    tree->Branch("column", &branches.column);
    tree->Branch("adc", &branches.adc);
  }
  void serialize()
  {
    this->nmodule = this->modules.size();
    for (auto module : this->modules)
    {
      branches.detid.push_back(module.detid);
      branches.module.push_back(module.module);
      branches.disk.push_back(module.disk);
      branches.layer.push_back(module.layer);
      branches.ring.push_back(module.ring);
      branches.side.push_back(module.side);

      branches.elink.push_back(module.elink);
      branches.lpgbt.push_back(module.lpgbt);
      branches.dtc.push_back(module.dtc);

      branches.row.push_back(module.row);
      branches.column.push_back(module.column);
      branches.adc.push_back(module.adc);
    }

    this->tree->Fill();
  }
};

class ITPixelExporter : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit ITPixelExporter(const edm::ParameterSet &);
  ~ITPixelExporter();

  static void fillDescriptions(edm::ConfigurationDescriptions &descriptions);

private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event &, const edm::EventSetup &) override;
  virtual void endJob() override;

  // ----------member data ---------------------------
  edm::EDGetTokenT<edm::DetSetVector<PixelDigi>> m_tokenDigis; // digi

  edm::ESGetToken<TrackerGeometry, TrackerDigiGeometryRecord> m_tokenGeometry; // geometry
  edm::ESGetToken<TrackerTopology, TrackerTopologyRcd> m_tokenTopo;            // topo
  edm::ESGetToken<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd> m_tokenMap;

  // the pointers to geometry, topology and clusters
  // these are members so all functions can access them without passing as
  // argument
  const TrackerTopology *tTopo = NULL;
  const TrackerGeometry *tkGeom = NULL;
  const edm::DetSetVector<PixelDigi> *digis = NULL;

  // File service to write ROOT output
  edm::Service<TFileService> m_fileservice;
  ITDigiEvent m_event;
  TTree *m_tree;
};

ITPixelExporter::ITPixelExporter(const edm::ParameterSet &iConfig) : m_tokenDigis(consumes<edm::DetSetVector<PixelDigi>>(iConfig.getParameter<edm::InputTag>("digis"))),
                                                                     m_tokenGeometry(esConsumes<TrackerGeometry, TrackerDigiGeometryRecord>(edm::ESInputTag("", "idealForDigi"))),
                                                                     m_tokenTopo(esConsumes<TrackerTopology, TrackerTopologyRcd>()),
                                                                     m_tokenMap(esConsumes<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd>())
{
  // TTree version 0: Save everything into one TTree
  // TODO: Organize data from different rings, detector parts
  // either into same TTree or different TTrees
  usesResource("TFileService");
  m_tree = m_fileservice->make<TTree>("Digis", "Digis");
  this->m_event.attach_tree(this->m_tree);
}

ITPixelExporter::~ITPixelExporter()
{

  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
}

//
// member functions
//

// ------------ method called for each event  ------------
void ITPixelExporter::analyze(const edm::Event &iEvent, const edm::EventSetup &iSetup)
{
  // Empty event container
  this->m_event.clear();
  this->m_event.event = iEvent.id().event();

  // get the digis - COB 26.02.19
  edm::Handle<edm::DetSetVector<PixelDigi>> tdigis;
  iEvent.getByToken(m_tokenDigis, tdigis);

  // Get the geometry
  edm::ESHandle<TrackerGeometry> tgeomHandle = iSetup.getHandle(m_tokenGeometry);

  // Get the topology
  edm::ESHandle<TrackerTopology> tTopoHandle = iSetup.getHandle(m_tokenTopo);

  // Get the cabling map
  edm::ESHandle<TrackerDetToDTCELinkCablingMap> cablingMapHandle = iSetup.getHandle(m_tokenMap);

  TrackerDetToDTCELinkCablingMap const *cablingMap = cablingMapHandle.product();

  // get the pointers to geometry and topology
  tTopo = tTopoHandle.product();
  const TrackerGeometry *tkGeom = &(*tgeomHandle);
  tkGeom = tgeomHandle.product();
  digis = tdigis.product(); // pointer to digis - COB 26.02.19

  // Loop over modules
  for (const edm::DetSet<PixelDigi> &DSV_it : *digis)
  {

    ITDigiModule imodule;

    // get the detid
    unsigned int rawid = DSV_it.detId();

    DetId detId(rawid);

    imodule.detid = rawid;

    // Determine whether it's barrel or endcap
    TrackerGeometry::ModuleType mType = tkGeom->getDetectorType(detId);

    if (!(mType == TrackerGeometry::ModuleType::Ph2PXF || mType == TrackerGeometry::ModuleType::Ph2PXF3D) && detId.subdetId() == PixelSubdetector::PixelEndcap)
      continue;

    // obtaining location of module
    if (tTopo->pxfDisk(detId) <= 8)
      continue;

    imodule.disk = tTopo->pxfDisk(detId);
    imodule.ring = tTopo->pxfBlade(detId);
    imodule.module = tTopo->pxfModule(detId);
    imodule.side = tTopo->pxfSide(detId);

    imodule.layer = 1;

    for (auto [min_id, max_id] : backside_panel_ids)
      imodule.layer = (detId > min_id && detId < max_id) ? 2 : imodule.layer;

    // Find the geometry of the module associated to the digi
    const GeomDetUnit *geomDetUnit(tkGeom->idToDetUnit(detId));

    if (!geomDetUnit)
      continue;

    // Loop over the digis in each module
    auto elink_ids = cablingMap->detIdToDTCELinkId(detId);

    imodule.elink = (*elink_ids.first).second.elink_id();
    imodule.lpgbt = (*elink_ids.first).second.gbtlink_id();
    imodule.dtc = (*elink_ids.first).second.dtc_id();

    imodule.hlt_nclusters = 0;

    if (!DSV_it.empty())
    {
      for (const PixelDigi &digit : DSV_it)
      {
        int32_t r = digit.row(), c = digit.column();

#ifdef REMOVE_EDGES_AND_CENTER

        if (r < ROW_CUTOFF / 2 && r >= (ROW_MAX - ROW_CUTOFF / 2))
          continue;
        if (c < COL_CUTOFF / 2 && c >= (COL_MAX - COL_CUTOFF / 2))
          continue;

        r -= ROW_CUTOFF / 2;
        c -= COL_CUTOFF / 2;

        if (r < 1 || r >= (ACTUAL_ROW_MAX - 1))
          continue;
        if (c < 1 || c >= (ACTUAL_COL_MAX - 1))
          continue;
        if (r >= (SECOND_CHIP_START_ROW - 1) && r < (SECOND_CHIP_START_ROW + 1))
          continue;
        if (c >= (SECOND_CHIP_START_COL - 1) && c < (SECOND_CHIP_START_COL + 1))
          continue;
#endif

        imodule.row.push_back(r);
        imodule.column.push_back(c);
        imodule.adc.push_back(digit.adc());
      }
    }

#ifdef DEBUG
    std::cout << "retrieved pixel data" << std::endl;
#endif

    this->m_event.modules.push_back(imodule);
  }

  this->m_event.serialize();
}

// ------------ method called once each job just before starting event loop
// ------------
void ITPixelExporter::beginJob() {}

// ------------ method called once each job just after ending the event loop
// ------------
void ITPixelExporter::endJob() {}

// ------------ method fills 'descriptions' with the allowed parameters for the
// module  ------------
void ITPixelExporter::fillDescriptions(
    edm::ConfigurationDescriptions &descriptions)
{
  // The following says we do not know what parameters are allowed so do no
  // validation
  //  Please change this to state exactly what you do use, even if it is no
  //  parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  // Specify that only 'tracks' is allowed
  // To use, remove the default given above and uncomment below
  // ParameterSetDescription desc;
  // desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  // descriptions.addDefault(desc);
}

// define this as a plug-in
DEFINE_FWK_MODULE(ITPixelExporter);
