#!/bin/bash

cd ../

set -e

cmsenv

user=$USER
initial=${user:0:1}
cernbox_dir=/eos/home-$initial/$user/itpixelexporter

for file in `ls ./data/datasets/*.txt`
do
	f=${file%.txt}
	f1=${f##*/}

	output_file=pixel_output_$f1.root
	
	echo $f1 run starting

	[ ! -d $cernbox_dir ] && mkdir $cernbox_dir

	cmsRun itpixelexporter/python/ITPixelExporter.py dataset=$f1 xrdredirector=file: output=$cernbox_dir/$output_file

done

