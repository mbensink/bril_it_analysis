# IT digi extraction for rate studies

The folder contains 3 plugins used to retrieve different data from CMSSW:

1. ITPixelExporter: exports the raw hits of the pixel detectors
2. ITHLTClusterexport: exports the clusters found by the HLT clustering algorithm
3. ITBRILClusterexporter: exports the clusters found by the BRIL clustering algorithm ([link here](https://gitlab.cern.ch/mbensink/bril_pixel_clustering))

## Setup

This code has to be run within LXPLUS ``{USERNAME}@lxplus.cern.ch``

```bash
CMSSW_VERSION="CMSSW_13_0_10"
cmsrel "${CMSSW_VERSION}"
cd "${CMSSW_VERSION}/src"
cmsenv
git clone https://gitlab.cern.ch/mbensink/bril_it_analysis.git
cd bril_it_analysis
scram b -j8
```

## Running

Every plugin uses the same input structure. example:

```bash
cmsRun {PLUGIN_FOLDER}/python/{PLUGIN_NAME}.py \
	xrdredirector=file: \
 	dataset={DATASETS} \
	output={OUTPUT_FILE_NAME}.root
```

### example

```bash
cmsRun ithltclusterexporter/python/ITHLTClusterExporter.py \
	xrdredirector=file: \
 	dataset=PU_75.0_D91_cbarrera \
	output=output_file_PU_75.0_cbarrera.root
```

### run all pileups

Every plugin contains a bash script that starts a run for every dataset within the dataset folder. The data will automatically be exported to the users cernbox folder.

[2GB datalimit!!!]

```bash
cd {PLUGIN_FOLDER}
bash get_all_data.sh

#or in background

cd {PLUGIN_FOLDER}
nohup bash get_all_data.sh > log.txt &

```

## Variables for retrieving a single module from the dataset

| Variable name | Data type          | Range                    | Explanation                                                       |
| ------------- | ------------------ | ------------------------ | ----------------------------------------------------------------- |
| side          | **uint8_t**  | 1 - 2                    | Side of the CMS detector (-z == 1 and +z == 2)                    |
| disk          | **uint8_t**  | 9 - 12                   | Which disk (12 is the furthest)                                   |
| ring          | **uint8_t**  | 1 - 5                    | Which ring within the disk (1 is most inner and 5 most outer)     |
| layer         | **uint8_t**  | 1 - 2                    | Which layer within the ring (1 is front layer and 2 is backlayer) |
| module        | **uint8_t**  | 1 - [10, 14, 18, 22, 24] | Module index (ring 1 has the least modules and ring 5 the most)   |
| detid         | **uint64_t** | -                        | The raw identification number of a module                         |

### Variables for pixel data

| variable name | data type          | range    | explanation               |
| ------------- | ------------------ | -------- | ------------------------- |
| column        | **uint16_t** | 0 - 434  | column value of the pixel |
| row           | **uint16_t** | 0 - 1355 | row value of the pixel    |
| adc           | **uint16_t** | -        | adc value                 |

### Variables for cluster data

| variable name  | data type          | range    | explanation                                   |
| -------------- | ------------------ | -------- | --------------------------------------------- |
| cluster_row    | **uint16_t** | 0 - 434  | column value of the cluster                   |
| cluster_col    | **uint16_t** | 0 - 1355 | row value of the cluster                      |
| cluster_size_x | **uint16_t** | -        | cluster width                                 |
| cluster_size_y | **uint16_t** | -        | cluster height                                |
| cluster_nBits  | **uint16_t** | -        | number of pixels contained within the cluster |

### Linking clusters to pixels

each pixel has a pixel cluster id. This id can be used to link the pixels back to which cluster it belongs to.

| variable name    | Data type          | explanation                            |
| ---------------- | ------------------ | -------------------------------------- |
| pixel_cluster_id | **uint16_t** | id of the cluster the pixel belongs to |
| cluster_id       | **uint16_t** | id of the cluster                      |

## Some notes:

- The dataset name must match to a file name in `./data/datasets`. The file should contain file paths to the input files for a given data set.
- When defining a new data set name, note that the format should always be `<PhysicsProcess>_<CMSSWVersion>_<DetectorGeometryTag>_<PileUpTag>`
- The choice of cabling map is made automatically based on the `DetectorGeometryTag` (D41 in the example above). Look at the `GEOMETRY_TO_CABLING` dictionary in the python config to see / change.
- Note that the cabling maps are automatically loaded from `./data/cabling/`. Make sure that your desired map is available there. If you need to create a new cabling sqlite file, go [here](https://github.com/AndreasAlbert/cmssw/tree/2021-09-30_itrate_cabling_maps/CondTools/SiPhase2Tracker/test).
- Make sure to use a CMSSW version that contains the geometry used in the MC sample being analyzed. The geometry files are periodically added / removed from CMSSW releases, so any releases that is either much newer or much older than the sample of interest will lead to problems. Additionally, CMSSW EDM files are not guaranteed to be readable by versions older than the one used to write the file.
- Form large datasets, use crab to process. See examples in the crab/ folder.
