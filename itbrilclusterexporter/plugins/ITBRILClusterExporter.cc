// -*- C++ -*-
//
// Package:    BRIL_ITsim/ITdigiExporter
// Class:      ITdigiExporter
//
/**\class ITdigiExporter ITdigiExporter.cc
BRIL_ITsim/ITdigiExporter/plugins/ITdigiExporter.cc

Description: [one line class summary]

Implementation:
[Notes on implementation]
*/
//
// Original Author:  Georg Auzinger
//         Created:  Thu, 17 Jan 2032 13:12:52 GMT
// Edits: Max Bensink
//
//

// #define DEBUG
// #define REMOVE_EDGES_AND_CENTER

// system include files
// system include files
#include <fstream>
#include <memory>
#include <algorithm>
#include <ranges>

// user include files
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "DataFormats/SiPixelCluster/interface/SiPixelCluster.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/ESInputTag.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "Geometry/CommonDetUnit/interface/GeomDet.h"
#include "Geometry/CommonDetUnit/interface/PixelGeomDetUnit.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"

#include "DataFormats/Common/interface/DetSetVector.h"
#include "DataFormats/Common/interface/DetSetVectorNew.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/DetId/interface/DetId.h"
#include "DataFormats/SiPixelDetId/interface/PixelSubdetector.h"
#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"
#include "DataFormats/TrackerCommon/interface/TrackerTopology.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "CommonTools/Utils/interface/TFileDirectory.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "CondFormats/DataRecord/interface/TrackerDetToDTCELinkCablingMapRcd.h"
#include "CondFormats/SiPhase2TrackerObjects/interface/DTCELinkId.h"
#include "CondFormats/SiPhase2TrackerObjects/interface/TrackerDetToDTCELinkCablingMap.h"

#include <TTree.h>

#include "ClusterModel.h"
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.
constexpr int32_t ROW_MAX = 1354;
constexpr int32_t COL_MAX = 434;
constexpr int32_t ROW_CUTOFF = 10;
constexpr int32_t COL_CUTOFF = 2;
constexpr int32_t ACTUAL_ROW_MAX = ROW_MAX - ROW_CUTOFF;
constexpr int32_t ACTUAL_COL_MAX = COL_MAX - COL_CUTOFF;

constexpr int32_t SECOND_CHIP_START_COL = ACTUAL_COL_MAX / 2;
constexpr int32_t SECOND_CHIP_START_ROW = ACTUAL_ROW_MAX / 2;

typedef std::pair<int, int> vertex_t;

struct ITDigiModule
{
  // side of the collider (1 == left and 2 == right)
  uint8_t side;
  // disk per side (max 4)
  uint8_t disk;
  // module per ring (max 25)
  uint8_t module;
  // layer per disk (1 == front and 2 == back)
  uint8_t layer;
  // ring per disk (max 5)
  uint8_t ring;
  // number of clusters per module
  uint32_t nclusters;
  // connected elink
  uint8_t elink;
  uint8_t lpgbt;
  uint16_t dtc;

  // id of module
  uint64_t detid;

  std::vector<unsigned short> row;    // The pixel row number on the module
  std::vector<unsigned short> column; // The pixel column number on the module
  std::vector<unsigned char> adc;     // ADC value for given pixel
  std::vector<unsigned short> pixel_cluster_id;

  std::vector<unsigned char> cluster_stage; // through which stage did this cluster go

  std::vector<unsigned short> cluster_row;
  std::vector<unsigned short> cluster_col;
  std::vector<unsigned short> cluster_size_x;
  std::vector<unsigned short> cluster_size_y;
  std::vector<unsigned short> cluster_nbits;
  std::vector<unsigned short> cluster_id;

  uint32_t col_not_adjacent;
  uint32_t col_already_used;
  uint32_t col_not_found;
  uint32_t col_rhs_must_be_bigger;
  uint32_t row_processor_buffer_not_empty;
  uint32_t row_already_used;
  uint32_t row_not_found;
};

std::vector<std::tuple<uint32_t, uint32_t>>
    backside_panel_ids({{348655e3, 348680e3},
                        {349175e3, 349205e3},
                        {349703e3, 349723e3},
                        {350227e3, 350248e3},
                        {357042e3, 357063e3},
                        {357567e3, 357588e3},
                        {358091e3, 358112e3},
                        {358616e3, 358637e3}});

struct ITDigiEvent
{
  // Struct to store address and ADC information about all Digis in an event

  long int event;
  unsigned int nmodule;

  TTree *tree = NULL;
  std::vector<ITDigiModule> modules; // The module number

  struct Branches
  {
    std::vector<unsigned int> detid;
    std::vector<unsigned char> module;
    std::vector<unsigned char> disk;
    std::vector<unsigned char> layer;
    std::vector<unsigned char> ring;

    std::vector<uint8_t> side;
    std::vector<uint8_t> elink;
    std::vector<uint8_t> lpgbt;
    std::vector<uint16_t> dtc;

    std::vector<std::vector<unsigned short>> row;    // The pixel row number on the module
    std::vector<std::vector<unsigned short>> column; // The pixel column number on the module
    std::vector<std::vector<unsigned char>> adc;     // ADC value for given pixel
    std::vector<std::vector<unsigned short>> pixel_cluster_id;

    std::vector<unsigned int> nclusters;
    std::vector<std::vector<unsigned short>> cluster_row;
    std::vector<std::vector<unsigned short>> cluster_col;
    std::vector<std::vector<unsigned short>> cluster_size_x;
    std::vector<std::vector<unsigned short>> cluster_size_y;
    std::vector<std::vector<unsigned short>> cluster_nbits;
    std::vector<std::vector<unsigned char>> cluster_stage;
    std::vector<std::vector<unsigned short>> cluster_id;

    std::vector<unsigned int> col_not_adjacent;
    std::vector<unsigned int> col_already_used;
    std::vector<unsigned int> col_not_found;
    std::vector<unsigned int> col_rhs_must_be_bigger;
    std::vector<unsigned int> row_processor_buffer_not_empty;
    std::vector<unsigned int> row_already_used;
    std::vector<unsigned int> row_not_found;
  } branches;

  void clear()
  {
    this->event = -1;
    this->nmodule = 0;
    this->modules.clear();

    branches = Branches();
  }

  void attach_tree(TTree *tree)
  {
    this->tree = tree;
    tree->Branch("event", &this->event, "event/I");
    tree->Branch("nmodule", &this->nmodule, "nmodule/I");

    tree->Branch("detid", &branches.detid);
    tree->Branch("module", &branches.module);
    tree->Branch("disk", &branches.disk);
    tree->Branch("layer", &branches.layer);
    tree->Branch("ring", &branches.ring);

    tree->Branch("side", &branches.side);
    tree->Branch("elink", &branches.elink);
    tree->Branch("lpgbt", &branches.lpgbt);
    tree->Branch("dtc", &branches.dtc);

    tree->Branch("row", &branches.row);
    tree->Branch("column", &branches.column);
    tree->Branch("adc", &branches.adc);
    tree->Branch("pixel_cluster_id", &branches.pixel_cluster_id);

    tree->Branch("nClusters", &branches.nclusters);

    tree->Branch("cluster_row", &branches.cluster_row);
    tree->Branch("cluster_col", &branches.cluster_col);
    tree->Branch("cluster_size_x", &branches.cluster_size_x);
    tree->Branch("cluster_size_y", &branches.cluster_size_y);
    tree->Branch("cluster_nBits", &branches.cluster_nbits);
    tree->Branch("cluster_stage", &branches.cluster_stage);
    tree->Branch("cluster_id", &branches.cluster_id);

    tree->Branch("col_not_adjacent", &branches.col_not_adjacent);
    tree->Branch("col_already_used", &branches.col_already_used);
    tree->Branch("col_not_found", &branches.col_not_found);
    tree->Branch("col_rhs_must_be_bigger", &branches.col_rhs_must_be_bigger);
    tree->Branch("row_processor_buffer_not_empty", &branches.row_processor_buffer_not_empty);
    tree->Branch("row_already_used", &branches.row_already_used);
    tree->Branch("row_not_found", &branches.row_not_found);
  }
  void serialize()
  {
    this->nmodule = this->modules.size();
    for (auto module : this->modules)
    {
      branches.detid.push_back(module.detid);
      branches.module.push_back(module.module);
      branches.disk.push_back(module.disk);
      branches.layer.push_back(module.layer);
      branches.ring.push_back(module.ring);
      branches.side.push_back(module.side);

      branches.elink.push_back(module.elink);
      branches.lpgbt.push_back(module.lpgbt);
      branches.dtc.push_back(module.dtc);

      branches.row.push_back(module.row);
      branches.column.push_back(module.column);
      branches.adc.push_back(module.adc);
      branches.pixel_cluster_id.push_back(module.pixel_cluster_id);

      branches.nclusters.push_back(module.nclusters);
      branches.cluster_col.push_back(module.cluster_col);
      branches.cluster_row.push_back(module.cluster_row);
      branches.cluster_size_x.push_back(module.cluster_size_x);
      branches.cluster_size_y.push_back(module.cluster_size_y);
      branches.cluster_nbits.push_back(module.cluster_nbits);
      branches.cluster_id.push_back(module.cluster_id);
      branches.cluster_stage.push_back(module.cluster_stage);

      branches.col_not_adjacent.push_back(module.col_not_adjacent);
      branches.col_already_used.push_back(module.col_already_used);
      branches.col_not_found.push_back(module.col_not_found);
      branches.col_rhs_must_be_bigger.push_back(module.col_rhs_must_be_bigger);
      branches.row_processor_buffer_not_empty.push_back(module.row_processor_buffer_not_empty);
      branches.row_already_used.push_back(module.row_already_used);
      branches.row_not_found.push_back(module.row_not_found);
    }

    this->tree->Fill();
  }
};

class ITBRILClusterExporter : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit ITBRILClusterExporter(const edm::ParameterSet &);
  ~ITBRILClusterExporter();

  static void fillDescriptions(edm::ConfigurationDescriptions &descriptions);

private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event &, const edm::EventSetup &) override;
  virtual void endJob() override;

  // ----------member data ---------------------------
  edm::EDGetTokenT<edm::DetSetVector<PixelDigi>> m_tokenDigis;                 // digi
  edm::ESGetToken<TrackerGeometry, TrackerDigiGeometryRecord> m_tokenGeometry; // geometry
  edm::ESGetToken<TrackerTopology, TrackerTopologyRcd> m_tokenTopo;            // geometry
  edm::ESGetToken<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd> m_tokenMap;

  // the pointers to geometry, topology and clusters
  // these are members so all functions can access them without passing as
  // argument
  const TrackerTopology *tTopo = NULL;
  const TrackerGeometry *tkGeom = NULL;
  const edm::DetSetVector<PixelDigi> *digis = NULL;

  // File service to write ROOT output
  edm::Service<TFileService> m_fileservice;
  ITDigiEvent m_event;
  TTree *m_tree;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//

ITBRILClusterExporter::ITBRILClusterExporter(const edm::ParameterSet &iConfig) : m_tokenDigis(consumes<edm::DetSetVector<PixelDigi>>(iConfig.getParameter<edm::InputTag>("digis"))),
                                                                                 m_tokenGeometry(esConsumes<TrackerGeometry, TrackerDigiGeometryRecord>(edm::ESInputTag("", "idealForDigi"))),
                                                                                 m_tokenTopo(esConsumes<TrackerTopology, TrackerTopologyRcd>()),
                                                                                 m_tokenMap(esConsumes<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd>())
{
  // TTree version 0: Save everything into one TTree
  // TODO: Organize data from different rings, detector parts
  // either into same TTree or different TTrees
  usesResource("TFileService");
  m_tree = m_fileservice->make<TTree>("Digis", "Digis");
  this->m_event.attach_tree(this->m_tree);
}

ITBRILClusterExporter::~ITBRILClusterExporter()
{

  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
}

//
// member functions
//

// ------------ method called for each event  ------------
void ITBRILClusterExporter::analyze(const edm::Event &iEvent, const edm::EventSetup &iSetup)
{
  // Empty event container
  this->m_event.clear();
  this->m_event.event = iEvent.id().event();

  // get the digis - COB 26.02.19
  edm::Handle<edm::DetSetVector<PixelDigi>> tdigis;
  iEvent.getByToken(m_tokenDigis, tdigis);

  // Get the geometry
  edm::ESHandle<TrackerGeometry> tgeomHandle = iSetup.getHandle(m_tokenGeometry);

  // Get the topology
  edm::ESHandle<TrackerTopology> tTopoHandle = iSetup.getHandle(m_tokenTopo);

  // Get the cabling map
  edm::ESHandle<TrackerDetToDTCELinkCablingMap> cablingMapHandle = iSetup.getHandle(m_tokenMap);

  TrackerDetToDTCELinkCablingMap const *cablingMap = cablingMapHandle.product();

  // get the pointers to geometry and topology
  tTopo = tTopoHandle.product();
  const TrackerGeometry *tkGeom = &(*tgeomHandle);
  tkGeom = tgeomHandle.product();
  digis = tdigis.product(); // pointer to digis - COB 26.02.19

  // Loop over modules
  for (const auto &DSV_it : *digis)
  {

    ITDigiModule imodule;

    // get the detid
    unsigned int rawid = DSV_it.detId();

    DetId detId(rawid);

    imodule.detid = rawid;

    // Determine whether it's barrel or endcap
    TrackerGeometry::ModuleType mType = tkGeom->getDetectorType(detId);

    if (!(mType == TrackerGeometry::ModuleType::Ph2PXF || mType == TrackerGeometry::ModuleType::Ph2PXF3D) && detId.subdetId() == PixelSubdetector::PixelEndcap)
      continue;

    // obtaining location of module
    if (tTopo->pxfDisk(detId) <= 8)
      continue;

    imodule.disk = tTopo->pxfDisk(detId);
    imodule.ring = tTopo->pxfBlade(detId);
    imodule.module = tTopo->pxfModule(detId);
    imodule.side = tTopo->pxfSide(detId);

    imodule.layer = 1;

    for (auto [min_id, max_id] : backside_panel_ids)
      imodule.layer = (detId > min_id && detId < max_id) ? 2 : imodule.layer;

    // Find the geometry of the module associated to the digi
    const GeomDetUnit *geomDetUnit(tkGeom->idToDetUnit(detId));

    if (!geomDetUnit)
      continue;

    // Loop over the digis in each module
    auto elink_ids = cablingMap->detIdToDTCELinkId(detId);

    imodule.elink = (*elink_ids.first).second.elink_id();
    imodule.lpgbt = (*elink_ids.first).second.gbtlink_id();
    imodule.dtc = (*elink_ids.first).second.dtc_id();

    imodule.nclusters = 0;

    std::map<vertex_t, int> pixel_mapping;

    size_t i = 0;

    if (!DSV_it.empty())
    {
      for (const auto &digit : DSV_it)
      {
        int32_t r = digit.row(), c = digit.column();

#ifdef REMOVE_EDGES_AND_CENTER

        if (r < ROW_CUTOFF / 2 && r >= (ROW_MAX - ROW_CUTOFF / 2))
          continue;
        if (c < COL_CUTOFF / 2 && c >= (COL_MAX - COL_CUTOFF / 2))
          continue;

        r -= ROW_CUTOFF / 2;
        c -= COL_CUTOFF / 2;

        if (r < 1 || r >= (ACTUAL_ROW_MAX - 1))
          continue;
        if (c < 1 || c >= (ACTUAL_COL_MAX - 1))
          continue;
        if (r >= (SECOND_CHIP_START_ROW - 1) && r < (SECOND_CHIP_START_ROW + 1))
          continue;
        if (c >= (SECOND_CHIP_START_COL - 1) && c < (SECOND_CHIP_START_COL + 1))
          continue;
#endif

        imodule.row.push_back(r);
        imodule.column.push_back(c);

        pixel_mapping[std::make_pair(c, r)] = i++;

        imodule.adc.push_back(digit.adc());
      }
    }

#ifdef DEBUG
    std::cout << "retrieved pixel data" << std::endl;
#endif

    std::map<vertex_t, QuarterCore> qcores_map;

    for (const auto &pix : pixel_mapping)
    {
      const auto [col, row] = pix.first;

      int32_t col_in_qcore = col % SIZE_QCORE_HORIZONTAL;
      int32_t row_in_qcore = row % SIZE_QCORE_VERTICAL;

      int32_t qcol = (col - col_in_qcore) / SIZE_QCORE_HORIZONTAL;
      int32_t qrow = (row - row_in_qcore) / SIZE_QCORE_VERTICAL;

      vertex_t qpos = std::make_pair(qcol, qrow);

      if (qcores_map.find(qpos) == qcores_map.end())
        qcores_map[qpos] = QuarterCore(qcol, qrow);

      qcores_map[qpos].add_hit(col_in_qcore, row_in_qcore);
    }

#ifdef DEBUG
    std::cout << "determined used qcores" << std::endl;
#endif

    std::vector<QuarterCore> qcores;

    for (const auto &item : qcores_map)
    {
      qcores.push_back(item.second);
    }

    auto qcores_metadata = determine_metadeta(qcores);

#ifdef DEBUG
    std::cout << "metadata is determined" << std::endl;
#endif

    std::vector<ProcessedQuarterCoreModel> qcore_processed;

    for (auto qcore : qcores_metadata)
      qcore_processed.push_back(ProcessedQuarterCoreModel(qcore));

#ifdef DEBUG
    std::cout << "Qcores are processed" << std::endl;
#endif

    DistributorModel distributor(qcore_processed);

    auto [single_clusters, row_buffer] = distributor.run();

#ifdef DEBUG
    std::cout << "Distributor is done" << std::endl;
#endif

    RowMergerModel row_merger(row_buffer);

    auto [row_clusters, col_buffer] = row_merger.run();

#ifdef DEBUG
    std::cout << "row merger is done" << std::endl;
#endif

    ColMergerModel col_merger(col_buffer);

    auto col_clusters = col_merger.run();

#ifdef DEBUG
    std::cout << "col merger is done" << std::endl;
#endif

    imodule.nclusters = single_clusters.size() + row_clusters.size() + col_clusters.size();

    imodule.cluster_id = std::vector<unsigned short>(imodule.adc.size(), 0);

#ifdef DEBUG
    std::cout << "bril hits array is allocated" << std::endl;
#endif

    auto fill_data = [&imodule, &pixel_mapping](const std::vector<ClusterModel> &data, char stage) -> void
    {
      uint32_t clu_id = 0;

      for (auto cluster : data)
      {

        std::vector<unsigned short> cluster_x, cluster_y;

        for (const auto &hit : cluster.hit_map)
        {
          const vertex_t global_coordinate = cluster.get_hit_global_coordinate(hit); // std::make_pair(x + SIZE_QCORE_HORIZONTAL * cluster.col, y + SIZE_QCORE_VERTICAL * cluster.row);

          cluster_x.push_back((uint16_t)global_coordinate.first);
          cluster_y.push_back((uint16_t)global_coordinate.second);

          if (pixel_mapping.find(global_coordinate) != pixel_mapping.end())
            imodule.cluster_id[pixel_mapping[global_coordinate]] = clu_id;
        }

        clu_id++;

        const auto mm_x = std::minmax_element(cluster_x.begin(), cluster_x.end());
        const auto mm_y = std::minmax_element(cluster_y.begin(), cluster_y.end());

        imodule.cluster_col.push_back(*mm_x.first);
        imodule.cluster_row.push_back(*mm_y.first);
        imodule.cluster_size_x.push_back(*mm_x.second - *mm_x.first + 1);
        imodule.cluster_size_y.push_back(*mm_y.second - *mm_y.first + 1);
        imodule.cluster_nbits.push_back(cluster.hit_map.size());
        imodule.cluster_stage.push_back(stage);
      }
    };

    fill_data(single_clusters, 1);

#ifdef DEBUG
    std::cout << "single_clusters is added to data" << std::endl;
#endif

    fill_data(row_clusters, 2);

#ifdef DEBUG
    std::cout << "row_clusters is added to data" << std::endl;
#endif

    fill_data(col_clusters, 3);

#ifdef DEBUG
    std::cout << "col_clusters is added to data" << std::endl;
#endif

    imodule.col_not_adjacent = col_merger.get_not_adjacent();
    imodule.col_already_used = col_merger.get_already_used();
    imodule.col_not_found = col_merger.get_not_found();
    imodule.col_rhs_must_be_bigger = col_merger.get_rhs_must_be_bigger();
    imodule.row_processor_buffer_not_empty = row_merger.get_processor_buffer_not_empty();
    imodule.row_already_used = row_merger.get_already_used();
    imodule.row_not_found = row_merger.get_not_found();

#ifdef DEBUG
    std::cout << "module cluster metadata is added to data" << std::endl;
#endif

    this->m_event.modules.push_back(imodule);
  }

  this->m_event.serialize();
}

// ------------ method called once each job just before starting event loop
// ------------
void ITBRILClusterExporter::beginJob() {}

// ------------ method called once each job just after ending the event loop
// ------------
void ITBRILClusterExporter::endJob() {}

// ------------ method fills 'descriptions' with the allowed parameters for the
// module  ------------
void ITBRILClusterExporter::fillDescriptions(
    edm::ConfigurationDescriptions &descriptions)
{
  // The following says we do not know what parameters are allowed so do no
  // validation
  //  Please change this to state exactly what you do use, even if it is no
  //  parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  // Specify that only 'tracks' is allowed
  // To use, remove the default given above and uncomment below
  // ParameterSetDescription desc;
  // desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  // descriptions.addDefault(desc);
}

// define this as a plug-in
DEFINE_FWK_MODULE(ITBRILClusterExporter);
