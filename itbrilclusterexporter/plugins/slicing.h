/**
 * @file slicing.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-18
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <vector>

// Slice a 1D vector similar to Python array slicing
template <typename T>
std::vector<T> slice(const std::vector<T> &arr, int start, int end, int step = 1)
{
    std::vector<T> result;

    if (step == 0)
    {
        throw std::runtime_error("Error: Step size cannot be zero.");
    }

    if (step > 0)
    {
        for (int i = start; i < end; i += step)
        {
            if (i >= 0 && i < (int)arr.size())
            {
                result.push_back(arr[i]);
            }
        }
    }
    else
    {
        for (int i = start; i > end; i += step)
        {
            if (i >= 0 && i < (int)arr.size())
            {
                result.push_back(arr[i]);
            }
        }
    }

    return result;
}

// Slice a 2D matrix similar to Python array slicing
template <typename T>
std::vector<std::vector<T>> slice_2d(const std::vector<std::vector<T>> &matrix,
                                     int rowStart, int rowEnd,
                                     int colStart, int colEnd,
                                     int rowStep = 1, int colStep = 1)
{
    std::vector<std::vector<T>> result;

    if (rowStep == 0 || colStep == 0)
    {
        throw std::runtime_error("Error: Step size cannot be zero.");
    }

    for (int i = rowStart; (rowStep > 0) ? i < rowEnd : i > rowEnd; i += rowStep)
    {
        if (i >= 0 && i < (int)matrix.size())
        {
            std::vector<T> rowSlice;
            for (int j = colStart; (colStep > 0) ? j < colEnd : j > colEnd; j += colStep)
            {
                if (j >= 0 && j < (int)matrix[i].size())
                {
                    rowSlice.push_back(matrix[i][j]);
                }
            }
            result.push_back(rowSlice);
        }
    }

    return result;
}