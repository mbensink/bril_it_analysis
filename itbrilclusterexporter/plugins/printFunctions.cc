

#include "ClusterModel.h"

/**
 * @brief prints out the hits within a single cluster
 * 
 * @param cluster cluster to print
 */
void print_sparsified_cluster(const ClusterModel &cluster)
{
    for (size_t y = 0; y < (size_t)SIZE_QCORE_VERTICAL * cluster.nrows; y++)
    {
        for (size_t x = 0; x < (size_t)SIZE_QCORE_HORIZONTAL * cluster.ncols; x++)
        {
            auto it = std::find(cluster.hit_map.begin(), cluster.hit_map.end(), std::make_pair<int, int>(x, y));

            std::cout << (it == cluster.hit_map.end() ? "0 " : "1 ");
        }
        std::cout << std::endl;
    }
}

/**
 * @brief prints out a set of clusters next to each other
 * 
 * @param clusters clusters to print
 */
void print_sparsified_clusters(const std::vector<ClusterModel> &clusters)
{
    for (size_t y = 0; y < (size_t)SIZE_QCORE_VERTICAL; y++)
    {
        for (size_t i = 0; i < clusters.size(); i++)
        {
            for (size_t x = 0; x < (size_t)SIZE_QCORE_HORIZONTAL; x++)
            {
                auto it = std::find(clusters[i].hit_map.begin(), clusters[i].hit_map.end(), std::make_pair<int, int>(x, y));

                std::cout << (it == clusters[i].hit_map.end() ? "0 " : "1 ");
            }
            std::cout << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

}


/**
 * @brief print out a quartercore
 * 
 * @param qcore the quartercore
 */
void print_quartercore(const QuarterCore &qcore)
{
    for (size_t y = 0; y < (size_t)SIZE_QCORE_VERTICAL; y++)
    {
        for (size_t x = 0; x < (size_t)SIZE_QCORE_HORIZONTAL; x++)
        {
            auto it = std::find(qcore.hit_map.begin(), qcore.hit_map.end(), std::make_pair<int, int>(x, y));

            std::cout << (it == qcore.hit_map.end() ? "0 " : "1 ");
        }
        std::cout << std::endl;
    }
    
    std::cout << std::endl;

}