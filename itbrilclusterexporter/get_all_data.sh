#!/bin/bash

cd ../

set -e

cmsenv

user=$USER
initial=${user:0:1}
cernbox_dir=/eos/home-$initial/$user/itbrilclusterexporter

for file in `ls ./data/datasets/*.txt`
do
	f=${file%.txt}
	f1=${f##*/}

	output_file=bril_output_$f1.root
	
	echo $f1 run starting

	[ ! -d $cernbox_dir ] && mkdir $cernbox_dir

	cmsRun itbrilclusterexporter/python/ITBRILClusterExporter.py dataset=$f1 xrdredirector=file: output=$cernbox_dir/$output_file
	
done

